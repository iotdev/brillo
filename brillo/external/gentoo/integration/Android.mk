LOCAL_PATH := $(call my-dir)

# Only load these subdirs when 3rd party packages have been requested.
ifneq ($(PRODUCT_3RD_PARTY_PACKAGES),)

ifeq ($(TARGET_IS_64_BIT),true)
3RD_PARTY_LIBDIR := lib64
else
3RD_PARTY_LIBDIR := lib
endif

3RD_PARTY_CC := $(HOST_OUT_EXECUTABLES)/3rd-party-gcc
3RD_PARTY_CXX := $(HOST_OUT_EXECUTABLES)/3rd-party-g++
3RD_PARTY_PKG_CONFIG := $(HOST_OUT_EXECUTABLES)/3rd-party-pkg-config

TARGET_OUT_GENTOO := $(TARGET_OUT_INTERMEDIATES)/gentoo
TARGET_OUT_COMMON_GENTOO := $(TARGET_OUT_COMMON_INTERMEDIATES)/gentoo
3RD_PARTY_ROOT := $(TARGET_OUT_GENTOO)/root
3RD_PARTY_ROOT_SUBDIR := $(patsubst $(PRODUCT_OUT)/%,%,$(3RD_PARTY_ROOT))

# These are system packages that we assume are always available before we can
# compile anything at all.  Basically we need a functional C/C++ compiler.
3RD_PARTY_SYSTEM_DEPS := libc libc++ libdl libm libstdc++

# We have to include subdirs first as they set up some vars we use below.
3RD_PARTY_CONFIGS :=
3RD_PARTY_WRAPPERS :=
include $(call all-subdir-makefiles)

# Some libraries are available in external/ already.
# Note: Keep this in sync with gentoo/integration/portage/package.provided.in.
# We do not list packages that do not provide libs/headers.
3RD_PARTY_EXTERNAL_LIBS := \
	libavahi-client libavahi-common libavahi-core \
	libbz \
	libcrypto libssl \
	libcares \
	libcurl \
	libdbus \
	libext2_blkid libext2_com_err libext2_e2p libext2fs libext2_quota libext2_uuid libtune2fs \
	libdw libdwelf libdwfl libebl libelf \
	libexpat \
	libFLAC \
	libgmock \
	libgtest \
	libicui18n libicuuc \
	libiprouteutil libnetlink \
	libip4tc libip6tc libxtables \
	libjemalloc \
	libjpeg \
	libcap \
	libcap-ng \
	libdaemon \
	libdivsufsort libdivsufsort64 \
	libevent \
	libmicrohttpd \
	libnl \
	libogg \
	libopus \
	libselinux \
	libunwind \
	libvpx libwebm \
	libpcre libpcrecpp \
	libsepol \
	libspeex \
	libtinyxml \
	libtinyxml2 \
	libwpa_client \
	libz \
	$(NULL)
# Only depend on the packages that are actually requested.
3RD_PARTY_EXTERNAL_DEPS += $(filter $(3RD_PARTY_EXTERNAL_LIBS),$(product_MODULES) $(PRODUCT_PACKAGES))

# Actually run the emerge command for the specific package.
%.emerge: TARGET_TO_ATOM = $(notdir $(patsubst %/,%,$(dir $(1))))/$(notdir $(1:.emerge=))
%.emerge: | $(3RD_PARTY_WRAPPERS) $(3RD_PARTY_CONFIGS) $(3RD_PARTY_EXTERNAL_DEPS)
	$(hide)mkdir -p $(dir $@) $(TARGET_OUT_GENTOO)/tmp
	$(hide)emerge $(call TARGET_TO_ATOM,$@)
	$(hide)3rd-party-merge \
		--package $(call TARGET_TO_ATOM,$@) \
		--input-root $(3RD_PARTY_ROOT) \
		--output-root $(TARGET_OUT) \
		--make-root $(TARGET_OUT_GENTOO)/mk-installed
	$(hide)touch $@

# Set up a module for each requested package.
define 3rd-party-merge
include $(CLEAR_VARS)
LOCAL_MODULE := mk-installed/$(1).emerge
LOCAL_MODULE_CLASS := GENTOO
LOCAL_UNINSTALLABLE_MODULE := true
include $$(BUILD_SYSTEM)/base_rules.mk
endef
$(foreach p,$(PRODUCT_3RD_PARTY_PACKAGES),$(eval $(call 3rd-party-merge,$(p))))

# A pseudo target that depends on all the generated packages above.
# The board can use this in their PRODUCT_PACKAGES list.
include $(CLEAR_VARS)
LOCAL_MODULE := 3rd-party-packages
LOCAL_MODULE_CLASS := GENTOO
LOCAL_ADDITIONAL_DEPENDENCIES := $(foreach p,$(PRODUCT_3RD_PARTY_PACKAGES),mk-installed/$(p).emerge)
include $(BUILD_SYSTEM)/base_rules.mk
LOCAL_BUILT_MODULE := $(intermediates)/$(LOCAL_MODULE)
$(LOCAL_BUILT_MODULE): $(LOCAL_ADDITIONAL_DEPENDENCIES)
	$(hide)mkdir -p $(dir $@)
	$(hide)touch $@

endif
