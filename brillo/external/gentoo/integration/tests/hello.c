/* Simple test program for using libiniparser. */

#undef NDEBUG
#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <string.h>

#include <iniparser.h>

int main(int argc, char *argv[])
{
	/* mktemp modifies the arg, so string has to be on the stack. */
	char tmp[] = "foo.XXXXXX";
	const char *file;
	FILE *fp;
	dictionary *dict;
	const char *val;

	file = mktemp(tmp);
	fp = fopen(file, "we");
	if (!fp)
		err(1, "could not open %s", file);
	fputs(
		"[section]\n"
		"key = value\n",
		fp);
	fclose(fp);

	dict = iniparser_load(file);
	if (!dict)
		err(1, "could not read %s", file);
	val = iniparser_getstring(dict, "section:key", NULL);
	if (strcmp(val, "value") != 0)
		errx(1, "section.key is '%s' but should be '%s'",
			val, "value");
	iniparser_freedict(dict);

	unlink(file);

	puts("success!");

	return 0;
}
