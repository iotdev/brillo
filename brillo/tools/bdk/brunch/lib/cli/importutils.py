#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Brunch CLI import utils"""

import imp
import inspect
import os
import uuid

def LoadModule(import_name, path):
  module_dir, module_name = os.path.split(path)
  mod_info = imp.find_module(module_name, [module_dir])
  try:
    f, file_path, items = mod_info
    module = imp.load_module(import_name, f, file_path, items)
    return module
  finally:
    if f:
      f.close()

def LoadModuleRandName(path):
  rand_name = uuid.uuid4().hex
  return LoadModule(rand_name, path)

def LoadPackage(path):
  return LoadModuleRandName(path)

def GetFilesInPath(path):
  for root, dirs, files in os.walk(path):
    for f in files:
      yield os.path.join(root, f)

def ModuleIter(path):
  modules = []
  for f in GetFilesInPath(path):
    modname = inspect.getmodulename(f)
    if not modname:
      continue
    if modname == '__init__':
      continue
    if modname in modules:
      continue
    modules.append(modname)
    yield LoadModuleRandName(os.path.join(path, modname))
