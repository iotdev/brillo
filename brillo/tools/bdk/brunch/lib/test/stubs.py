#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""As needed, hand-coded stubs for writing tests in lib/"""

import copy
import os
import string

from core import util

class StubShutil(object):
  """Replacement for shutil.* as needed"""
  os = None
  Error = Exception

  @classmethod
  def rmtree(cls, directory):
    cls.os.path.should_exist = [path for path in cls.os.path.should_exist if
                           not path.startswith(directory)]
    cls.os.path.should_be_dir = [path for path in cls.os.path.should_be_dir if
                           not path.startswith(directory)]

  @classmethod
  def copytree(cls, start, end):
    cls.os.path.should_exist += [path.replace(start, end, 1)
                                 for path in cls.os.path.should_exist
                                 if path.startswith(start)]
    cls.os.path.should_be_dir += [path.replace(start, end, 1)
                                  for path in cls.os.path.should_be_dir
                                  if path.startswith(start)]


class StubOs(object):
  """Replacement for os.* as needed"""
  environ = { }
  pathsep = ':'

  should_makedirs = []
  @classmethod
  def makedirs(cls, path):
    if path in cls.should_makedirs:
      cls.path.should_be_dir.append(path)
      cls.path.should_exist.append(path)
      return True
    raise OSError, 'computer says no'

  @classmethod
  def symlink(cls, source, link):
    if cls.path.exists(link):
      raise OSError, 'Link name already exists'
    if not cls.path.exists(source):
      raise OSError, 'Link target does not exist'
    cls.path.should_link[link] = source

  @classmethod
  def readlink(cls, path):
    if path in cls.path.should_link:
      return cls.path.should_link[path]
    raise OSError, 'Bad argument to readlink - missing or non-link'

  @classmethod
  def remove(cls, f):
    while f in cls.path.should_exist:
      cls.path.should_exist.remove(f)
    while f in cls.path.should_link:
      del cls.path.should_link[f]

  @classmethod
  def walk(cls, root):
    # Note: this does not match the specs of os.walk, but is
    # close enough for our purposes.
    return [(p[:p.rfind('/')], None, [p.split('/')[-1]]) for
            p in cls.path.should_exist if p.startswith(root)]

  system_commands = []
  system_returns = []
  @classmethod
  def system(cls, cmdline):
    ret = cls.system_returns.pop(0)
    cls.system_commands.append(cmdline)
    return ret

  class path(object):
    should_exist = []
    should_be_dir = []
    should_link = {}

    @classmethod
    def exists(cls, path):
      return path in cls.should_exist

    @classmethod
    def islink(cls, path):
      return path in cls.should_link

    @classmethod
    def isfile(cls, path):
      return path in cls.should_exist and not path in cls.should_be_dir

    @classmethod
    def samefile(cls, path1, path2):
      """Subject to infinite loops with cyclical links."""
      while path1 in cls.should_link:
        path1 = cls.should_link[path1]
      while path2 in cls.should_link:
        path2 = cls.should_link[path2]
      if not path1 in cls.should_exist:
        raise OSError, "{0} does not exist".format(path1)
      elif not path2 in cls.should_exist:
        raise OSError, "{0} does not exist".format(path2)
      else:
        return path1 == path2

    @classmethod
    def join(cls, *components):
      return os.path.join(*components)

    @classmethod
    def dirname(cls, f):
      return os.path.dirname(f)

    @classmethod
    def isdir(cls, f):
      return f in cls.should_be_dir

    @classmethod
    def relpath(cls, sub, root):
      # For stub purposes, assume they are actually subpaths; no ..
      return sub[(len(root) + 1):] # + 1 to cut out the '/'

    @classmethod
    def abspath(cls, p):
      return os.path.abspath(p)


class StubFile(object):
  """Stub for filling in for a file object"""
  def __init__(self, contents=''):
    self.contents = string.split(contents, '\n')
    self.line = 0

  def __iter__(self):
    return self

  def next(self):
    """Return the next line"""
    result = self.readline()
    if not result:
      raise StopIteration
    return result

  def readline(self):
    """Return the next line"""
    if self.line >= len(self.contents):
      return ''

    suffix = '\n'
    if len(self.contents) -1 == self.line:
      suffix = '';
    result = self.contents[self.line] + suffix
    self.line += 1
    return result

  def read(self):
    return string.join(self.contents, '\n')

  def write(self, contents):
    """Currently clobbers all contents and doesn't fix iters."""
    self.contents = string.split(contents, '\n')


class StubHashlib(object):
  """Wraps the hashlib object."""
  should_return = None

  class StubHash(object):
    def __init__(self, should_return):
      self.should_return = should_return

    def hexdigest(self):
      return self.should_return

  @classmethod
  def sha256(cls, data):
    return cls.StubHash(cls.should_return)


class StubOpen(object):
  """Wraps both the open() object and the special handler"""
  os = None
  files = {}

  def __init__(self, path, files, *args):
    self.files = files
    self.path = path
    if 'w' in args:
      self.os.path.should_exist.append(self.path)

  @classmethod
  def open(cls, path, *args):
    return cls(path, cls.files, *args)

  def file(self):
    if self.os.path.exists(self.path) and self.files.has_key(self.path):
      return self.files[self.path]
    raise IOError, 'File does not exist'

  def __enter__(self):
    return self.file()

  def __exit__(self, type, value, traceback):
    if value is not None:
      raise type, value
    return True


class StubSubprocess(object):
  """Wraps a subprocess object.

  Attributes:
    signature - list of words that combined uniquely identify
                this particular subprocess. e.g. ["git", "clone"].
    input_args - arguments who's values are input files.
    output_args - arguments who's values are output files.
    positional_inputs - index of positional arguments who's values are input files.
    positional_outputs - index of positional arguments who's values are output files.
    side_effect_creates - additional files that should be created when this
                          subprocess is run.
    return_out - output to out when this subprocess is run.
    return_err - output to err when this subprocess is run.
    return_code - return code of the subprocess when run.
  """
  def __init__(self, os, opener, signature, in_args=None, out_args=None, pos_in=None,
               pos_out=None, ret_out=None, ret_err=None, ret_code=0):
    self._os = os
    self._open = opener
    self._args = []
    self.signature = signature
    self.input_args = in_args or []
    self.output_args = out_args or []
    self.positional_inputs = pos_in or []
    self.positional_outputs = pos_out or []
    self.return_out = ret_out or None
    self.return_err = ret_err or None
    # The code that should be set once the subprocess is complete
    self.return_code = ret_code
    # The returncode attribute of the subprocess
    self.returncode = -1

  def getArgByName(self, name):
    return self._args[self._args.index(name) + 1]

  def start(self, args):
    # Make them nicer for processing
    for arg in args:
      self._args += arg.split('=')

  def communicate(self):
    # Check that all inputs exist
    for req in self.input_args:
      req_arg = self.getArgByName(req)
      if not self._os.path.exists(req_arg):
        return ("OUTPUT", "ERROR: input %s to subprocess missing" % req_arg)
    for req_index in self.positional_inputs:
      if not self._os.path.exists(self._args[req_index]):
        return ("OUTPUT", "ERROR: input %s to subprocess missing" % self._args[req_index])

    # "Create" all outputs
    for out in self.output_args:
      out_arg = self.getArgByName(out)
      self._os.path.should_exist.append(out_arg)
      self._open.files[out_arg] = StubFile()
    for out_index in self.positional_outputs:
      out_arg = self._args[out_index]
      self._os.path.should_exist.append(out_arg)
      self._open.files[out_arg] = StubFile()

    # Set return code
    self.returncode = self.return_code
    return (self.return_out, self.return_err)

  call_parameters = []
  call_returns = []
  @classmethod
  def call(cls, arg_array, env={}, cwd=None, shell=False):
    cls.call_parameters.append({'args':arg_array, 'env':env, 'cwd':cwd, 'shell':shell})
    return cls.call_returns.pop(0)


class StubClone(StubSubprocess):
  def __init__(self):
    super(self, StubClone).__init__(['git', 'clone'], pos_out=[-1], ret_code=-1)

  def communicate(self):
    ret = super(self, StubClone).communicate()
    self._os.path.should_exist.append(self._os.path.join(self._args[-1], '.git'))
    return ret


class StubPopener(object):
  """Wraps Popen calls"""
  def __init__(self, os, opener):
    self._os = os
    self._open = opener
    self.commands = []

  def AddCommand(self, signature, in_args=None, out_args=None, pos_in=None,
                 pos_out=None, ret_out=None, ret_err=None, ret_code=0):
    self.commands.append(StubSubprocess(self._os, self._open, signature, in_args, out_args,
                                        pos_in, pos_out, ret_out, ret_err, ret_code))

  def PopenPiped(self, args):
    for command in self.commands:
      if set(command.signature).issubset(set(args)):
        command.start(args)
        return command


class StubStdout(object):
  """Replacement for Stdout for easy access to what was printed."""

  def __init__(self):
    self.printed = ""

  def write(self, string):
    self.printed += string


class StubFactory(object):
  """A simple copy.copy wrapper for stub classes"""
  @classmethod
  def new(cls, obj):
    return copy.copy(obj)

class StubUtil(object):
  """A stub for core/util.py"""
  bdk_path = '/some/bdk/path'
  dev_tools_path = bdk_path + '/dev/tools'
  @classmethod
  def GetBDKPath(cls):
    return cls.bdk_path

  @classmethod
  def GetDevToolsPath(cls):
    return cls.dev_tools_path

  @classmethod
  def AsShellArgs(cls, ary):
    return util.AsShellArgs(ary)

  @classmethod
  def GetExitCode(cls, retval):
    if retval == 0:
      return 0
    else:
     return 1
