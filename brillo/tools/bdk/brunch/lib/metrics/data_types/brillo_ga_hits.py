#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""This module defines classes for Brillo GA hits.

Wrapper classes for GA hits are provided that fill in general Brillo
information, and provide high level constructors so that users of this
module can send relevant hits without needing to worry about correctly
formatting them to match expectations.
"""

import multiprocessing
import os
import platform

import generalized_ga
from core import util
from core import config

_BDK_VERSION_FILE = util.GetDevToolsPath('VERSION')

_BRILLO_APP_ID = 'UA-67119306-1'
_BRILLO_APP_NAME = 'brunch'
_BRILLO_CD_OS = 1
_BRILLO_CD_OS_VERSION = 2
_BRILLO_CD_CPU_CORES = 3
_BRILLO_CD_CPU_SPEED = 4
_BRILLO_CD_RAM = 5
_BRILLO_CD_BDK = 6
_BRILLO_PROTOCOL_VERSION = 1

def _UserID():
  return config.UserStore().uid or 0


def _BDKVersion():
  return util.GetBDKVersion()


def _GetOSVersion():
  """Gets the OS release and major revision"""
  return '.'.join(platform.release().split('.')[:2])


def _GetCPUSpeed():
  """Gets the CPU speed of the first core."""
  speed = 'unknown'
  try:
    for line in open('/proc/cpuinfo'):
      if 'model name' in line:
        speed = line.split('@')[1].strip()
        break
  except:
    pass
  return speed


def _GetRAM():
  """Gets the amount of physical memory in the system in GB.

  This function rounds down to the nearest byte.
  """
  ram = 'unknown'
  try:
    ram = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES') / (1024**3)
  except (AttributeError, ValueError):
    # AttributeError: os.sysconf is only defined on unix systems.
    # ValueError: configuration names are not recognized (shouldn't happen,
    #   but better safe than sorry).
    pass
  return ram


def _MetaData():
  return generalized_ga.MetaData(_BRILLO_PROTOCOL_VERSION,
                                 _BRILLO_APP_ID,
                                 _BRILLO_APP_NAME,
                                 _UserID())


def _CustomDimensions():
  cds = {}
  cds[_BRILLO_CD_OS] = platform.system()
  cds[_BRILLO_CD_OS_VERSION] = _GetOSVersion()
  cds[_BRILLO_CD_CPU_CORES] = multiprocessing.cpu_count()
  cds[_BRILLO_CD_CPU_SPEED] = _GetCPUSpeed()
  cds[_BRILLO_CD_RAM] = _GetRAM()
  cds[_BRILLO_CD_BDK] = _BDKVersion()
  return cds


class BuildEvent(generalized_ga.Event):
  """An event to be sent when a build occurs.

  A build event uses the following protocol:
    category: "build"
    action:   [build time]
    value:    [build time]
    label:    [build type]
    cds: standard Brillo custom dimensions
  """

  def __init__(self, build_type, time):
    super(BuildEvent, self).__init__(_MetaData(),
                                     "build",
                                     time,
                                     time,
                                     build_type,
                                     _CustomDimensions())


class BuildTiming(generalized_ga.Timing):
  """A timing to be sent when a build occurs.

  A build timing uses the following protocol:
    category: "build"
    variable: [build time]
    time:     [build time]
    label:    [build type]
    cds: standard Brillo custom dimensions
  """

  def __init__(self, build_type, time):
    super(BuildTiming, self).__init__(_MetaData(),
                                      "build",
                                      time,
                                      time,
                                      build_type,
                                      _CustomDimensions())


class InstallEvent(generalized_ga.Event):
  """An event to be sent when an install occurs.

  An install event uses the following protocol:
    category: "install"
    action:   [install time]
    value:    [install time]
    cds: standard Brillo custom dimensions
  """

  def __init__(self, time):
    super(InstallEvent, self).__init__(_MetaData(),
                                       "install",
                                       time,
                                       time,
                                       custom_dimensions=_CustomDimensions())


class InstallTiming(generalized_ga.Timing):
  """A timing to be sent when an install occurs.

  An install timing uses the following protocol:
    category: "install"
    variable: [install time]
    time: [install time]
    cds: standard Brillo custom dimensions
  """

  def __init__(self, time):
    super(InstallTiming, self).__init__(_MetaData(),
                                        "install",
                                        time,
                                        time,
                                        custom_dimensions=_CustomDimensions())
