#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Change Metrics Opt-in/Out"""

from cli import clicommand
from core import config

class Metrics(clicommand.Command):
  """Check or control what data you are sending to the Brillo team."""

  STATUS = 'status'
  ENABLE = 'enable'
  DISABLE = 'disable'

  @staticmethod
  def Args(parser):
    parser.add_argument('action', choices=[Metrics.STATUS, Metrics.ENABLE, Metrics.DISABLE],
                        nargs='?', default=None, help='Check or change your metrics opt-in status')

  def Run(self, args):
    if not args.action or args.action == self.STATUS:
      self._Check_Status()
    elif args.action == self.ENABLE:
      self._Enable()
    elif args.action == self.DISABLE:
      self._Disable()
    else:
      # Should never get here if parser is working correctly
      print 'Unrecognized option,', args.action
      return 1
    return 0

  def _Check_Status(self):
    if config.UserStore().metrics_opt_in != '0':
      print 'You are currently opted into sending metrics to the Brillo team.'
      print 'To opt out, run:'
      print '  brunch config metrics disable'
    else:
      print 'You are currently opted out of sending metrics to the Brillo team.'
      print 'To opt in, run:'
      print '  brunch config metrics enable'

  def _Disable(self):
    config.UserStore().metrics_opt_in = False
    print 'You have opted out of sending metrics to the Brillo team.'
    print 'To opt in again in the future, run:'
    print '  brunch config metrics enable'

  def _Enable(self):
    config.UserStore().metrics_opt_in = True
    print 'You have opted in to sending metrics to the Brillo team.'
    print 'To opt out at any point in the future, run:'
    print '  brunch config metrics disable'
