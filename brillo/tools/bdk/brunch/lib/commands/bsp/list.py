#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""List BSPs"""

from bsp import manifest_reader
from cli import clicommand

class List(clicommand.Command):
  """List available BSPs, and their current installation status."""

  @staticmethod
  def Args(parser):
    parser.add_argument('-v', '--verbose', action='store_true',
                        help="Print detailed information of device subpackages")

  def Generate(self, manifest_file, verbose, bdk):
    """Generates a list of devices and statuses.

    Returns:
      A (sorted) list of (short name, status string) tuples
    """
    manifest = manifest_reader.Read(manifest_file, bdk)

    devices = []

    for short_name in sorted(manifest.devices):
      device = manifest.devices[short_name]
      (_, status_string) = device.Status(verbose)
      devices.append((short_name, status_string))

    return devices

  def Print(self, devices):
    """Prints a list of devices and statuses.

    Args:
      devices - a list of (short name, status string) tuples
    """
    for device_details in devices:
      print '%s - %s' % device_details

  def Run(self, args):
    devices = self.Generate(args.manifest, args.verbose, args.bdk)
    self.Print(devices)
    return 0
