#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Build a Product"""

import argparse
import os
import time

from cli import clicommand
from commands.product import constants
from core import config
from core import tool
from core import util
from metrics.data_types import brillo_ga_hits
from metrics import send_hits

class Build(clicommand.Command):
  """Build a product project from the current directory"""

  @staticmethod
  def Args(parser):
    parser.add_argument('-s', '--submodule', action='store_true',
                        help='Build the submodule in the current directory')
    parser.add_argument('-p', '--product_path', default=util.GetProductDir(), required=False,
                        help='Path to the root of the product')
    parser.add_argument('-b', '--buildtype', default=None, required=False,
                        choices=['eng', 'userdebug', 'user'],
                        help='Override the configured type of build to perform')
    parser.add_argument('make_args', nargs=argparse.REMAINDER,
                        help='Arguments to pass through to make')

  def Run(self, args):
    sdk_path = util.GetDevToolsPath()

    if args.product_path is None:
      print constants.MSG_NO_PRODUCT_PATH
      return 1

    store = config.ProductFileStore(args.product_path)

    # Pull the buildtype from the config if it is not overridden.
    if args.buildtype is None:
      args.buildtype = store.bdk.buildtype

    no_java = 'BRILLO_NO_JAVA=0'
    if store.bdk.java != '1':
      no_java = 'BRILLO_NO_JAVA=1'

    # This should only happen if the ProductFileStore is incomplete.
    if not args.buildtype:
      args.buildtype = 'userdebug'

    # Don't pass along the first '--' since it was meant for argparse.
    args.make_args.count('--') and args.make_args.remove('--')

    # If user options haven't been set up yet, do so.
    user_store = config.UserStore()
    if not user_store.complete():
      user_store.initialize()

    envcmd = 'make'
    start_time = time.time()

    make = tool.ToolWrapper(store, args.product_path, 'make')
    wrap_makefile = os.path.join(sdk_path, 'build/wrap-product.mk')
    buildtype = 'BUILDTYPE=%s' % args.buildtype
    if args.submodule == True:
       print "Building submodule only . . ."
       here = 'HERE=%s' % os.getcwd()
       ret = make.run(['-f', wrap_makefile,
                       no_java, here, buildtype,
                      'ENVCMD=mm'] + args.make_args)
       envcmd = 'mm'
    else:
       # Change to the product path and build the whole product.
       make.set_cwd(args.product_path)
       ret = make.run(['-f', wrap_makefile,
                       no_java, buildtype] + args.make_args)

    end_time = time.time()
    build_time = int(end_time - start_time)

    # Determine the envcmd for reporting purposes
    for arg in args.make_args:
      split_arg = arg.split('=')
      if split_arg[0] == 'ENVCMD' and len(split_arg) > 1:
        envcmd = split_arg[1]

    # TODO(arihc)(b/25952600): also check for not --help,
    #   since this is what build_time > 0 is primarily checking for,
    #   and the wrapper runs lunch first, making basically all builds > 0 time.
    if user_store.metrics_opt_in == '1' and ret == 0 and build_time > 0:
      send_hits.SendHitAndRetries(brillo_ga_hits.BuildTiming(envcmd,
                                                             build_time))

    return ret
