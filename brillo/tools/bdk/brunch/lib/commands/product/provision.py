#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Wraps provision-device"""

import argparse
import os

from cli import clicommand
from commands.product import constants
from core import config
from core import tool
from core import util

class Provision(clicommand.Command):
  """Run provision-device for a given product"""

  @staticmethod
  def Args(parser):
    parser.add_argument('-p', '--product_path', default=util.GetProductDir(),
                        help='Path to the root of the product')
    parser.add_argument('args', nargs=argparse.REMAINDER,
                        help='Arguments to pass through to the tool')

  def Run(self, args):
    if args.product_path is None:
      print constants.MSG_NO_PRODUCT_PATH
      return 1

    store = config.ProductFileStore(args.product_path)

    t = tool.TargetToolWrapper(store, args.product_path, 'provision-device')
    if not t.exists():
      print 'The product must be built once prior to using provision.'
      return 1

    # We have to export the host tools because provision-device
    # expects them in its path.
    ht = tool.HostToolWrapper(store, args.product_path, 'dummy')
    # Grab the tool path from the dummy tool.
    path_str = '%s%s' % (os.path.dirname(ht.path()), os.pathsep)
    if t.environment.has_key('PATH'):
      # The ToolWrapper merges in the calling environment, so
      # we should always just be prefixing.
      t.environment['PATH'] = path_str + t.environment['PATH']
    else:
      print 'No PATH found in the environment! Please set a PATH and call again'
      return 1
    return t.run(args.args)
