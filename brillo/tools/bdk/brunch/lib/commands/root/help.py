#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Prints help"""

from cli import clicommand

class Help(clicommand.Command):
  """Print help for a brunch command"""

  @staticmethod
  def Args(parser):
    parser.add_argument('command', help="Command for help")

  def Run(self, args):
    print "Help"
    # TODO(leecam)(b/25952606): Implement or remove
    return 0
