#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Brunch util functions"""

import os
import string

import core

ANDROID_PRODUCTS_MK = 'AndroidProducts.mk'
_BRUNCH_BASE = None

def GetBrunchConfigDir():
  global _BRUNCH_BASE
  if _BRUNCH_BASE:
    return _BRUNCH_BASE
  if os.environ.get('BRILLO_CONFIG_DIR'):
    path = os.environ['BRILLO_CONFIG_DIR']
    if not os.path.isdir(path):
      # TODO(leecam)(b/25952666) Change to BrunchException
      raise
  else:
    basedir = os.path.expanduser("~")
    path = os.path.join(basedir, '.brillo')
    if not os.path.isdir(path):
      os.makedirs(path)
  _BRUNCH_BASE = path
  return path

def GetBDKPath(relpath = None):
  """Find the base of the BDK"""
  core_path = os.path.dirname(core.__file__)
  result = os.path.abspath(os.path.join(core_path,
      '..', '..', '..', '..', '..'))
  if relpath:
    result = os.path.join(result, relpath)
  return result

def GetDevToolsPath(relpath = None):
  result = GetBDKPath('tools/bdk')
  if relpath:
    result = os.path.join(result, relpath)
  return result

USER_DB = os.path.join(GetBrunchConfigDir(), 'config.db')
METRICS_DB = os.path.join(GetBrunchConfigDir(), 'metrics.db')

def GetBDKVersion():
  """Find the BDK version"""
  # Cache the version.
  # TODO(wad)(b/25952145) make this less hacky.
  if vars().get('bdk_version') is not None:
    return vars().get('bdk_version')
  core_path = os.path.dirname(core.__file__)
  version_path = os.path.abspath(os.path.join(core_path,
      '..', '..', '..', 'VERSION'))
  version = 0  # Default version.
  with open(version_path, 'r') as f:
    version = f.readline().strip()
  vars()['bdk_version'] = version
  return version

def GetProductDir():
   """Walks from cwd upward to find the product root"""
   p = os.getcwd()
   while p != '/':
     if os.path.isfile(os.path.join(p, ANDROID_PRODUCTS_MK)):
       return p
     p = os.path.abspath(os.path.join(p, '..'))
   return None

def AsShellArgs(args):
  # TODO(wad)(b/25952900) we should sanitize beyond just quotes.
  return ' '.join(['"%s"' % a.replace('"', '\\"') for a in args])

def GetExitCode(status):
  """Convert an os.wait status code to a shell return value"""
  if os.WIFEXITED(status):
    return os.WEXITSTATUS(status)
  if os.WIFSTOPPED(status):
    return 128+os.WSTOPSIG(status)
  if os.WIFSIGNALED(status):
    return 128+os.WTERMSIG(status)
  return 1
