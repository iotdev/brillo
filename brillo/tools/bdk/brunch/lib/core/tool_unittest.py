#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for tool.py"""

import unittest

from core import config
from core import tool
from core import util
from test import stubs


class ToolWrapperTest(unittest.TestCase):
  def setUp(self):
    tool.util = stubs.StubFactory.new(stubs.StubUtil)
    tool.os = stubs.StubFactory.new(stubs.StubOs)
    tool.subprocess = stubs.StubFactory.new(stubs.StubSubprocess)

    self.fake_store_cls = stubs.StubFactory.new(config.DictStore)
    self.fake_store_cls.REQUIRED_PROPS = config.ProductFileStore.REQUIRED_PROPS
    self.fake_store_cls.OPTIONAL_PROPS = config.ProductFileStore.OPTIONAL_PROPS

    self.store = self.fake_store_cls()
    self.store.device = 'somedevice'
    self.product_path = 'product_path'

  def tearDown(self):
    pass

class TestToolRun(ToolWrapperTest):
  def test_base(self):
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    t.environment.clear()
    t.environment['PATH'] = 'abc:123'
    t.environment['BAZ'] = 'B "A" R'
    tool.subprocess.call_returns.append(0)
    tool.subprocess.call_returns.append(1)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(p['args'][0], '/my/tool')
    self.assertEqual(p['args'][1], 'arg1')
    self.assertEqual(p['shell'], False)
    self.assertEqual(p['cwd'], None)
    self.assertEqual(p['env']['PATH'], 'abc:123')
    self.assertEqual(p['env']['BAZ'], 'B "A" R')
    self.assertEqual(1, t.run(['arg1']))
    tool.subprocess.call_parameters.pop(0)

  def test_sigabrt(self):
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    tool.subprocess.call_returns.append(-6)
    self.assertEqual((128+6), t.run(['arg1']))
    tool.subprocess.call_parameters.pop(0)

  def test_cwd(self):
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    t.set_cwd('/here')
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(p['cwd'], '/here')


class TestToolEnvironment(ToolWrapperTest):
  def test_baseline(self):
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    t.environment.clear()
    t.environment['PATH'] = 'abc:123'
    t.environment['BAZ'] = 'B "A" R'
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(p['args'][0], '/my/tool')
    self.assertEqual(p['args'][1], 'arg1')
    self.assertEqual(p['env']['PATH'], 'abc:123')
    self.assertEqual(p['env']['BAZ'], 'B "A" R')

  def test_no_path(self):
    """Ensure no exceptions if there is no PATH"""
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual('/some/bdk/path/dev/tools/brunch', p['env']['PATH'])

  def test_untouchable(self):
    """Test config/bdk/allowed_environ limits"""
    self.store.bdk.allowed_environ = 'BDK_PATH'
    tool.os.environ['BDK_PATH'] = '/path/to/crazy/town'

    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(p['env']['BDK_PATH'], '/some/bdk/path')

  def test_passthrough(self):
    """Ensure config/bdk/allowed_environ is respected"""
    self.store.bdk.allowed_environ = 'FUZZY WUZZY'
    tool.os.environ['FUZZY'] = 'WUZ'
    tool.os.environ['WUZZY'] = 'A BEAR'

    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(p['env']['FUZZY'], tool.os.environ['FUZZY'])
    self.assertEqual(p['env']['WUZZY'], tool.os.environ['WUZZY'])
    self.assertEqual(p['env']['BDK_PATH'], '/some/bdk/path')

  def test_product_out(self):
    """Ensure ANDROID_PRODUCT_OUT meets the contract from adb."""
    t = tool.ToolWrapper(self.store, self.product_path, '/my/tool')
    tool.subprocess.call_returns.append(0)
    self.assertEqual(0, t.run(['arg1']))
    p = tool.subprocess.call_parameters.pop(0)
    self.assertEqual(
      'product_path/out/out-somedevice/target/product/somedevice',
      p['env']['ANDROID_PRODUCT_OUT'])


class TestHostToolWrapper(ToolWrapperTest):
  def test_path(self):
    t = tool.HostToolWrapper(self.store, self.product_path, 'tool')
    self.assertEqual(
      'product_path/out/out-somedevice/host/linux-x86/bin/tool',
      t.path())
    t = tool.HostToolWrapper(self.store, self.product_path, 'fastboot',
                             arch='win-mips')
    self.assertEqual(
      'product_path/out/out-somedevice/host/win-mips/bin/fastboot',
      t.path())

class TestTargetToolWrapper(ToolWrapperTest):
  def test_path(self):
    t = tool.TargetToolWrapper(self.store, self.product_path, 'tool')
    self.assertEqual(
      'product_path/out/out-somedevice/target/product/somedevice/tool',
      t.path())
    t = tool.TargetToolWrapper(self.store, self.product_path, 'provision-dev')
    self.assertEqual(
      'product_path/out/out-somedevice/target/product/somedevice/provision-dev',
      t.path())
