#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in manifest.py"""

import unittest

import manifest

class ManifestTest(unittest.TestCase):
  def setUp(self):
    self.base_manifest = {
      'packages' : {
        'package_1' : {
          'package_type' : 'git',
          'remote' : 'remote',
          'version' : 'version',
          'subpackages' : {
            'subpackage_1' : {
              'subdir' : '.',
              'licenses' : ['license1', 'license2']
            }
          }
        }
      },
      'devices' : {
        'device_1' : {
          'device_name' : 'Test Device 1',
          'packages' : {
            'package_1' : {
              'subpackage_1' : 'path/to/extract'
            }
          }
        }
      }
    }

  def test_from_dict(self):
    man = manifest.Manifest.FromDict(self.base_manifest)
    self.assertIsInstance(man, manifest.Manifest)

  def test_from_dict_incomplete_devices(self):
    incomplete_manifest = self.base_manifest
    incomplete_manifest['devices']['device_2'] = {}
    self.assertRaisesRegexp(KeyError, 'device_2', manifest.Manifest.FromDict, incomplete_manifest)

  def test_from_dict_incomplete_packages(self):
    incomplete_manifest = self.base_manifest
    incomplete_manifest['packages']['package_2'] = {}
    self.assertRaisesRegexp(KeyError, 'package_2', manifest.Manifest.FromDict, incomplete_manifest)

  def test_from_dict_missing_devices(self):
    incomplete_manifest = self.base_manifest
    del incomplete_manifest['devices']
    self.assertRaises(KeyError, manifest.Manifest.FromDict, incomplete_manifest)

  def test_from_dict_missing_packages(self):
    incomplete_manifest = self.base_manifest
    del incomplete_manifest['packages']
    self.assertRaises(KeyError, manifest.Manifest.FromDict, incomplete_manifest)

  def test_minimal_manifest(self):
    minimal_manifest = { 'packages' : {}, 'devices' : {} }
    man = manifest.Manifest.FromDict(minimal_manifest)
    self.assertIsInstance(man, manifest.Manifest)
    self.assertEqual(len(man.packages), 0)
    self.assertEqual(len(man.devices), 0)
