#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Provides a class for BSP devices.

Also includes a hook function to decode dictionaries into
Device objects.
"""

import os
import shutil
import tempfile

import package
import status
from core import util

DEVICE_KEY_NAME = 'device_name'
DEVICE_KEY_PACKAGES = 'packages'

class Device(object):
  """Class to represent devices with BSPs available.

  Attributes:
    name - the name of the package.
    _package_map - a mapping of { package.Package : { subpackage_name : path } },
      informing which subpackages of which packages this device requires, and where
      in the bdk it expects them to be installed.
  """

  def __init__(self, name, package_map, bdk=None):
    """Initializes a Device.

    Args:
      name - the name of the device.
      package_map - a mapping of { package.Package : { subpackage_name : relative_path } },
        where relative_path is relative to the BDK root.
      bdk - (optional) The path of the BDK to use for operations involving this device.
        Default ''.
    """
    bdk = bdk or ''
    self.name = name
    self._package_map = {}
    for (package, subpackage_map) in package_map.iteritems():
      self._package_map[package] = {}
      for (subpackage, relative_path) in subpackage_map.iteritems():
        self._package_map[package][subpackage] = os.path.join(bdk, relative_path)

  def Status(self, verbose=False):
    """Checks the status of a device.

    Args:
      verbose - (optional) If True, also return details of subpackages.

    Returns:
      (status, status string), where where status is the highest status
      of any subpackage that is part of the device BSP, or status.INSTALLED
      if the BSP has no subpackages. The status string lists the device and
      human-readable status, and in verbose mode also includes the same for
      each subpackage.
    """
    device_status = status.INSTALLED
    packages_string = ''
    for (pkg, subpackage_map) in self._package_map.iteritems():
      packages_string += '\n * {0}'.format(pkg.name)
      for (subpackage, subpackage_path) in subpackage_map.iteritems():
        (subpackage_status, subpackage_string) = pkg.SubPackageStatus(subpackage,
                                                                      subpackage_path)
        packages_string += '\n    * {0}'.format(subpackage_string)
        device_status = max(device_status, subpackage_status)

    device_string = '{0} - {1}'.format(self.name, status.StatusString(device_status))
    if verbose:
      device_string += packages_string

    return (device_status, device_string)

  def Cleanup(self):
    """Cleans up the installation of this device.

    Removes INCORRECT_VERSION and INVALID subpackages.
    """
    for (pkg, subpackage_map) in self._package_map.iteritems():
      for (subpackage, subpackage_path) in subpackage_map.iteritems():
        subpackage_status = pkg.SubPackageStatus(subpackage, subpackage_path)[0]
        if subpackage_status == status.INCORRECT_VERSION or subpackage_status == status.INVALID:
          pkg.RemoveSubPackage(subpackage, subpackage_path)

  def UnrecognizedPaths(self):
    """Returns a list of paths of unrecognized subpackages of this device.
    """
    result = []
    for (pkg, subpackage_map) in self._package_map.iteritems():
      for (subpackage, subpackage_path) in subpackage_map.iteritems():
        if pkg.SubPackageStatus(subpackage, subpackage_path)[0] == status.UNRECOGNIZED:
          result.append(subpackage_path)
    return result

  def MatchTarball(self, tarball):
    """Matches a tarball to its corresponding package.

    Args:
      tarball - a path to a tarball file
    Returns:
      The name of the matching package, if any.
      None otherwise.
    """
    tar_hash = package.TarPackage.GetTarballVersion(tarball)
    for pkg in self._package_map:
      if tar_hash.startswith(pkg.version):
        return pkg.name

    return None

  def Install(self, extract_only=None, auto_accept=False):
    """Installs the BSP for this device.

    Args:
      extract_only - (optional) a map of { package_name : tarball_file },
                     for packages to skip the tarball download step.
      auto_accept - (optional) True to accept all licenses automatically.

    Returns:
      True if the installation is successful, False otherwise.
    """
    extract_only = extract_only or {}
    for (pkg, subpackage_map) in self._package_map.iteritems():
      # Only install packages/subpackages in need of installation
      needs_installation = { subpackage : path for
                             (subpackage, path) in subpackage_map.iteritems() if
                             pkg.SubPackageStatus(subpackage, path)[0] == status.NOT_INSTALLED }
      if len(needs_installation) == 0:
        continue

      # We use extract_only.get() so that None is passed by default.
      if pkg.Download(extract_only.get(pkg.name), auto_accept):
        # Only try linking if download was successful.
        for (subpackage, path) in needs_installation.iteritems():
          pkg.LinkSubPackage(subpackage, path)

    # Once all packages have attempted download and linking,
    # print and return the results.
    (device_status, status_string) = self.Status(verbose=True)
    print status_string
    if device_status == status.INSTALLED:
      print 'Successfully installed all packages for {0}.'.format(self.name)
      return True
    elif device_status == status.UNRECOGNIZED:
      print 'The following paths exist but do not link to BSP packages:'
      for path in self.UnrecognizedPaths():
        print ' * ' + path
      print 'If this is not intentional, consider removing them and installing again.'
      return True
    else:
      print 'Failed to install some packages for {0}.'.format(self.name)
      return False

  def Refresh(self, extract_only=None):
    """Refreshes all BSP packages for a device."""
    extract_only = extract_only or {}
    result = True
    for pkg in self._package_map:
      result = (result and pkg.Refresh(extract_only.get(pkg.name)))
    return result

  @classmethod
  def FromDict(cls, dic, packages, bdk=None):
    """Create a Device from a dict.

    Merges together the name-based package identification with
    assembled Package objects.

    Args:
      dic - the dictionary to build the device from.
      packages - a dictionary mapping { package_name : Package }
      bdk - (optional) the root of the bdk for this device.
    """
    name = dic[DEVICE_KEY_NAME]
    package_map = {}
    for (package_name, subpackage_map) in dic[DEVICE_KEY_PACKAGES].iteritems():
      pkg = packages.get(package_name)
      if not pkg:
        raise ValueError, 'Package {0} for {1} does not exist.'.format(package_name, name)
      for subpackage in subpackage_map:
        if not subpackage in pkg.subpackages:
          raise ValueError, 'Subpackage {0}.{1} for {2} does not exist.'.format(package_name,
                                                                                subpackage,
                                                                                name)
      package_map[pkg] = subpackage_map

    return cls(name, package_map, bdk)
