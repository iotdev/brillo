#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in package.py"""

import unittest

from core import util
from test import stubs
import package
import status
import subpackage

class PackageTest(unittest.TestCase):
  def setUp(self):
    self.stub_os = stubs.StubFactory.new(stubs.StubOs)
    self.stub_open = stubs.StubFactory.new(stubs.StubOpen)
    self.stub_open.os = self.stub_os
    self.stub_hashlib = stubs.StubFactory.new(stubs.StubHashlib)
    self.stub_shutil = stubs.StubFactory.new(stubs.StubShutil)
    self.stub_shutil.os = self.stub_os
    self.stub_stdout = stubs.StubStdout()

    package.os = self.stub_os
    package.open = self.stub_open.open
    package.shutil = self.stub_shutil
    package.hashlib = self.stub_hashlib
    package.sys.stdout = self.stub_stdout

    self.stub_popener = stubs.StubPopener(self.stub_os, self.stub_open)
    self.subpackages = { 'subpackage1' : subpackage.SubPackage('subpackage1', 'subdir1',
                                                               ['license1', 'license2']),
                         'subpackage2' : subpackage.SubPackage('subpackage2', 'sub/dir2',
                                                               ['license2', 'license3']) }
    self.pkg = package.Package('name', 'remote', 'version',
                               self.subpackages, self.stub_popener)

    # Special packages for downloading
    self.tarPackage = package.TarPackage('name', 'remote', 'hash',
                                         self.subpackages, self.stub_popener)
    self.gitPackage = package.GitPackage('name', 'remote', 'branch:hash',
                                         self.subpackages, self.stub_popener)
    # Stub their license checking
    def stub_license_check(subpackage, auto_accept):
      return auto_accept
    self.tarPackage._CheckSubPackageLicenses = stub_license_check
    self.gitPackage._CheckSubPackageLicenses = stub_license_check

  # Helper for subpackage status / remove
  def setup_installed_subpackage(self, name, path):
    subdir_path = self.stub_os.path.join(self.pkg.directory, self.pkg.subpackages[name].subdir)
    self.stub_os.path.should_exist.append(path)
    self.stub_os.path.should_exist.append(subdir_path)
    self.stub_os.path.should_link[path] = subdir_path

  # Helper for subpackage status / remove
  def setup_broken_link_subpackage(self, name, path):
    subdir_path = self.stub_os.path.join(self.pkg.directory, self.pkg.subpackages[name].subdir)
    self.stub_os.path.should_link[path] = subdir_path

  # Helper for subpackage status / remove
  def setup_non_link_subpackage(self, name, path):
    subdir_path = self.stub_os.path.join(self.pkg.directory, self.pkg.subpackages[name].subdir)
    self.stub_os.path.should_exist.append(path)

  # Helper for subpackage remove
  def assertSubPackageRemoved(self, name, path):
    self.assertFalse(self.stub_os.path.exists(path))
    self.assertFalse(self.stub_os.path.islink(path))
    self.assertEqual(self.pkg.SubPackageStatus(name, path)[0], status.NOT_INSTALLED)

  def test_from_dict(self):
    package_dict = {
      'package_type' : 'git',
      'remote' : 'github',
      'version' : 'branch:deadbeef',
      'subpackages' : {
        'subpackage_1' : {
          'subdir' : 'sub1',
          'licenses' : ['license.txt']
        },
        'subpackage_2' : {
          'subdir' : 'sub2',
          'licenses' : []
        }
      }
    }
    pkg = package.Package.FromDict(package_dict, 'package_1')
    self.assertIsInstance(pkg, package.Package)
    self.assertEqual(len(pkg.subpackages), 2)
    self.assertTrue('subpackage_1' in pkg.subpackages)
    self.assertEqual(pkg.remote, 'github')
    self.assertEqual(pkg.version, 'branch:deadbeef')

    # Check git vs. tar packages.
    self.assertIsInstance(pkg, package.GitPackage)
    package_dict['package_type'] = 'tar'
    pkg = package.Package.FromDict(package_dict, 'package_1')
    self.assertIsInstance(pkg, package.TarPackage)

  def test_from_dict_invalid(self):
    package_dict = {
      'package_type' : 'not_git_or_tar',
      'remote' : 'github',
      'version' : 'branch:deadbeef',
      'subpackages' : {
        'subpackage_1' : {
          'subdir' : 'sub1',
          'licenses' : ['license.txt']
        },
        'subpackage_2' : {
          'subdir' : 'sub2',
          'licenses' : []
        }
      }
    }
    # Type must be git or tar.
    self.assertRaises(ValueError, package.Package.FromDict, package_dict, 'package_1')

  def test_subpackage_status_nonexistent_subpackage(self):
    # Should get some sort of error about 'not_a_subpackage' not being a real subpackage of pkg.
    self.assertRaisesRegexp(ValueError, 'not_a_subpackage',
                            self.pkg.SubPackageStatus, 'not_a_subpackage', 'link')

  def test_subpackage_status_installed(self):
    self.setup_installed_subpackage('subpackage1', 'link')
    self.assertEqual(self.pkg.SubPackageStatus('subpackage1', 'link')[0], status.INSTALLED)

  def test_subpackage_status_broken_link(self):
    self.stub_os.path.should_exist = []
    self.setup_broken_link_subpackage('subpackage1', 'link')
    self.assertEqual(self.pkg.SubPackageStatus('subpackage1', 'link')[0], status.INVALID)

  def test_subpackage_status_non_link(self):
    self.setup_non_link_subpackage('subpackage1', 'not_a_link')
    self.assertEqual(self.pkg.SubPackageStatus('subpackage1', 'not_a_link')[0], status.UNRECOGNIZED)

  def test_subpackage_status_no_link(self):
    self.assertEqual(self.pkg.SubPackageStatus('subpackage1', 'non_existant')[0],
                     status.NOT_INSTALLED)

  def test_subpackage_status_incorrect_version(self):
    self.setup_installed_subpackage('subpackage1', 'link')
    self.pkg.directory = 'different_version'
    self.assertEqual(self.pkg.SubPackageStatus('subpackage1', 'link')[0], status.INCORRECT_VERSION)

  def test_remove_nonexistent_subpackage(self):
    # Should get some sort of error about 'not_a_subpackage' not being a real subpackage of pkg.
    self.assertRaisesRegexp(ValueError, 'not_a_subpackage',
                            self.pkg.RemoveSubPackage, 'not_a_subpackage', 'link')

  def test_remove_installed_subpackage(self):
    self.setup_installed_subpackage('subpackage1', 'link')
    self.pkg.RemoveSubPackage('subpackage1', 'link')
    self.assertSubPackageRemoved('subpackage1', 'link')

  def test_remove_broken_link_subpackage(self):
    self.setup_broken_link_subpackage('subpackage1', 'link')
    self.pkg.RemoveSubPackage('subpackage1', 'link')
    self.assertSubPackageRemoved('subpackage1', 'link')

  def test_remove_non_link_subpackage(self):
    self.setup_non_link_subpackage('subpackage1', 'link')
    self.pkg.RemoveSubPackage('subpackage1', 'link')
    self.assertSubPackageRemoved('subpackage1', 'link')

  def test_remove_no_link_subpackage(self):
    self.pkg.RemoveSubPackage('subpackage1', 'link')
    self.assertSubPackageRemoved('subpackage1', 'link')

  def test_tarball_version(self):
    file1 = stubs.StubFile('file1')
    self.stub_os.path.should_exist = ['file1']
    self.stub_open.files = { 'file1' : file1 }
    self.stub_hashlib.should_return = '<hash_of_file1>'
    self.assertEqual(package.TarPackage.GetTarballVersion('file1'),
                     '<hash_of_file1>')

  def test_already_downloaded(self):
    self.stub_os.path.should_be_dir = [self.pkg.directory]
    self.assertTrue(self.pkg.Download())

  def test_git_download(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.gitPackage.directory]
    # Note: we don't test that tmp_dir/.git exists, because that's
    # hard to write cleanly; so we assume it is created and exists
    # as expected by clone/rev-parse.
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['git', 'clone'], pos_out=[-1])
    self.stub_popener.AddCommand(['git', 'rev-parse'], ret_out='hash')
    self.assertTrue(self.gitPackage.Download(auto_accept=True))
    self.assertTrue(self.stub_os.path.exists(self.gitPackage.directory))

  def test_git_no_license_accept(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.gitPackage.directory]
    # Note: we don't test that tmp_dir/.git exists, because that's
    # hard to write cleanly; so we assume it is created and exists
    # as expected by clone/rev-parse.
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['git', 'clone'], pos_out=[-1])
    self.stub_popener.AddCommand(['git', 'rev-parse'], ret_out='hash')
    self.assertFalse(self.gitPackage.Download(auto_accept=False))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.gitPackage.directory))

  def test_git_failed_download(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.gitPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['git', 'clone'], pos_out=[-1], ret_code=-1)
    self.assertFalse(self.gitPackage.Download(auto_accept=True))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.gitPackage.directory))

  def test_git_wrong_version(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.gitPackage.directory]
    # Note: we don't test that tmp_dir/.git exists, because that's
    # hard to write cleanly; so we assume it is created and exists
    # as expected by clone/rev-parse.
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['git', 'clone'], pos_out=[-1])
    self.stub_popener.AddCommand(['git', 'rev-parse'], ret_out='wronghash')
    self.assertFalse(self.gitPackage.Download(auto_accept=True))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.gitPackage.directory))

  def test_git_tarball(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.gitPackage.directory]
    # Some sort of error about no tarballs for git packages
    self.assertRaisesRegexp(NotImplementedError, 'tarball', self.gitPackage.Download, 'file', True)
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.gitPackage.directory))

  def test_tar_download(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['curl'], out_args=['-o'])
    # Again, side effects of popen are hard to encode, so this doesn't
    # reflect that the '-C' output directory will get populated by tar.
    self.stub_popener.AddCommand(['tar'], in_args=['-C', '-xzf'])
    self.stub_hashlib.should_return = self.tarPackage.version
    self.assertTrue(self.tarPackage.Download(auto_accept=True))
    self.assertTrue(self.stub_os.path.exists(self.tarPackage.directory))

  def test_tar_no_license_accept(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['curl'], out_args=['-o'])
    # Again, side effects of popen are hard to encode, so this doesn't
    # reflect that the '-C' output directory will get populated by tar.
    self.stub_popener.AddCommand(['tar'], in_args=['-C', '-xzf'])
    self.stub_hashlib.should_return = self.tarPackage.version
    self.assertFalse(self.tarPackage.Download(auto_accept=False))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.tarPackage.directory))

  def test_tar_failed_download(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['curl'], out_args=['-o'], ret_code=-1)
    self.stub_hashlib.should_return = self.tarPackage.version
    self.assertFalse(self.tarPackage.Download(auto_accept=True))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.tarPackage.directory))

  def test_tar_wrong_version(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['curl'], out_args=['-o'], ret_code=-1)
    self.stub_hashlib.should_return = 'wrong_version'
    self.assertFalse(self.tarPackage.Download(auto_accept=True))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.tarPackage.directory))

  def test_tar_bad_unzip(self):
    self.stub_os.path.should_exist = []
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    self.stub_popener.AddCommand(['curl'], out_args=['-o'])
    self.stub_popener.AddCommand(['tar'], in_args=['-C', '-xzf'], ret_code=-1)
    self.stub_hashlib.should_return = self.tarPackage.version
    self.assertFalse(self.tarPackage.Download(auto_accept=True))
    # Leave in a clean state
    self.assertFalse(self.stub_os.path.exists(self.tarPackage.directory))

  def test_tar_tarball(self):
    self.stub_os.path.should_exist = ['tar_file']
    self.stub_open.files['tar_file'] = stubs.StubFile()
    self.stub_os.path.should_be_dir = []
    self.stub_os.should_makedirs = [self.tarPackage.directory]
    self.stub_popener.commands = []
    # If it tries to actually download, should fail
    self.stub_popener.AddCommand(['curl'], out_args=['-o'], ret_code=-1)
    self.stub_popener.AddCommand(['tar'], in_args=['-C', '-xzf'])
    self.stub_hashlib.should_return = self.tarPackage.version
    self.assertTrue(self.tarPackage.Download('tar_file', auto_accept=True))
    self.assertTrue(self.stub_os.path.exists(self.tarPackage.directory))
    # Make sure the tar file is still where it was.
    self.assertTrue(self.stub_os.path.exists('tar_file'))

  def test_check_license(self):
    """Test license checking since we stub it as part of download testing."""
    self.stub_stdout.printed = ''
    license_texts = []
    (subpackage_name, subpackage) = self.pkg.subpackages.items()[0]
    subpackage_dir = self.stub_os.path.join(self.pkg.directory, subpackage.subdir)
    self.stub_os.path.should_be_dir = [self.pkg.directory, subpackage_dir]
    self.stub_os.path.should_exist.append(subpackage_dir)
    for license_file in subpackage.licenses:
      license_path = self.stub_os.path.join(subpackage_dir, license_file)
      license_text = 'license {0}.{1}.{2}'.format(self.pkg.name, subpackage.name, license_file)
      license_texts.append(license_text)
      self.stub_open.files[license_path] = stubs.StubFile(license_text)
      self.stub_os.path.should_exist.append(license_path)

    # Have some distractors to test that not everything gets picked up
    # (Note: this will obviously fail if 'license.txt' is the name of
    #  one of the expected test licenses)
    wrong_path = self.stub_os.path.join(self.pkg.directory, subpackage.subdir, 'license.txt')
    self.stub_open.files[wrong_path] = stubs.StubFile('wrong')
    self.stub_os.path.should_exist.append(wrong_path)

    # Test linking
    self.assertTrue(self.pkg._CheckSubPackageLicenses(subpackage.name, True))
    # All subdir licenses should be read
    for license_text in license_texts:
      self.assertTrue(license_text in self.stub_stdout.printed)
    # Only listed subdir licenses should be read
    self.assertFalse('wrong' in self.stub_stdout.printed)

  def test_link_subpackage(self):
    self.stub_stdout.printed = ''
    self.stub_os.path.should_exist = []
    self.stub_os.should_link = {}
    (subpackage_name, subpackage) = self.pkg.subpackages.items()[0]
    subpackage_dir = self.stub_os.path.join(self.pkg.directory, subpackage.subdir)
    self.stub_os.path.should_be_dir = [self.pkg.directory, subpackage_dir]
    self.stub_os.path.should_exist.append(subpackage_dir)
    self.stub_os.should_link['link_origin'] = subpackage_dir

    # Test linking
    self.assertTrue(self.pkg.LinkSubPackage('subpackage1', 'link_origin'))
    # Correct link should be created
    self.assertEqual(self.stub_os.readlink('link_origin'),
                     subpackage_dir)
