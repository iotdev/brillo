/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <base/at_exit.h>
#include <base/logging.h>
#include <base/message_loop/message_loop.h>
#include <binder/IServiceManager.h>
#include <binder/Status.h>
#include <brillo/binder_watcher.h>
#include <brillo/syslog_logging.h>
#include <utils/String16.h>

#include "android/brillo/example/BnExampleService.h"
#include "android/brillo/example/IAlertCallback.h"

namespace android {

class ExampleService : public android::brillo::example::BnExampleService {
 public:
  binder::Status Log(const String16& s) {
    LOG(INFO) << String8(s).string();
    count_++;
    if (count_ == max_events_) {
      cbo_->OnNLogEventsReceivedCallback(count_);
      count_ = 0;
    }
    return binder::Status::ok();
  }

  binder::Status RegisterCallback(
      const sp<android::brillo::example::IAlertCallback>& callback, int n) {
    cbo_ = callback;
    max_events_ = n;
    count_ = 0;
    return binder::Status::ok();
  }

 private:
  sp<android::brillo::example::IAlertCallback> cbo_;
  int count_ = 0;
  int max_events_ = 0;
};

}  // namespace android

int main() {
  // TODO(ralphnathan): Hide this in brillo::BaseMessageLoop.
  base::AtExitManager at_exit_manager;
  brillo::InitLog(brillo::kLogToSyslog);
  // Register service with the service manager.
  android::status_t status = android::defaultServiceManager()->addService(
      android::String16("android.brillo.example.ExampleService"),
      new android::ExampleService());
  CHECK(status == android::OK) <<
      "Failed to get IExampleService binder from service manager!";

  // Create a message loop.
  // TODO(ralphnathan): Change this to brillo::BaseMessageLoop.
  base::MessageLoopForIO message_loop_for_io;

  // Initialize a binder watcher.
  brillo::BinderWatcher watcher;
  watcher.Init();

  // Run the message loop.
  message_loop_for_io.Run();
}
